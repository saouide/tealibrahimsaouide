package ma.teal.saouide.ibrahim.minifacebook.activities;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import ma.teal.saouide.ibrahim.minifacebook.R;
import ma.teal.saouide.ibrahim.minifacebook.activities.Base.BaseActivity;
import ma.teal.saouide.ibrahim.minifacebook.tools.Store;

public class PhotoActivity extends BaseActivity {

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

    }

    private void initView() {
        setContentView(R.layout.activity_photo);
        setupToolbar();

        image = findViewById(R.id.model_image);
        Picasso.with(this.getBaseContext()).load(Store.getInstance().photo.url).placeholder(R.drawable.placeholder).into(image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu_nav, menu);
        menu.getItem(0).setIcon(R.drawable.ic_close_black_24dp);
        Drawable drawable = menu.getItem(0).getIcon();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        }
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_menu_1:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
