package ma.teal.saouide.ibrahim.minifacebook.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ma.teal.saouide.ibrahim.minifacebook.R;
import ma.teal.saouide.ibrahim.minifacebook.activities.Base.BaseActivity;
import ma.teal.saouide.ibrahim.minifacebook.models.Photo;
import ma.teal.saouide.ibrahim.minifacebook.tools.Store;

public class PhotosActivity extends BaseActivity {

    Context mContext;
    RecyclerView listView;
    PhotoAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    List<Photo> photoList = new ArrayList<>();
    List<Photo> photoListToStore = new ArrayList<>();
    TextView name;
    RelativeLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        if (AccessToken.getCurrentAccessToken() == null) {
            redirectTo(LoginActivity.class, true);
        } else {
            getPhotos();
        }
        isWriteStoragePermissionGranted();
    }

    private void initView() {
        mContext = this.getBaseContext();
        setContentView(R.layout.activity_photos);
        initCollapsingToolbar();
        setupToolbar();
        name = findViewById(R.id.model_name);
        name.setText(Store.getInstance().album.name);

        progressBar = findViewById(R.id.progressBar);

        photoListToStore.clear();

        listView = findViewById(R.id.list_item);
        listView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(mContext, 2);
        listView.setLayoutManager(layoutManager);
        adapter = new PhotoAdapter(photoList);
        listView.setAdapter(adapter);
    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(Store.getInstance().album.name);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu_nav, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_menu_1:
                storePhotos();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void storePhotos() {
        if (isWriteStoragePermissionGranted()) {
            progressBar.setVisibility(View.VISIBLE);
            for (Photo photo : photoListToStore) {
                try {
                    URL imageurl = new URL(photo.url);
                    Bitmap bitmap = BitmapFactory.decodeStream(imageurl.openConnection().getInputStream());

                    MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "image1", null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), R.string.store_done, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoHolder> {

        List<Photo> photoList;

        class PhotoHolder extends RecyclerView.ViewHolder {
            CheckBox checkBox;
            ImageView image;

            PhotoHolder(View view) {
                super(view);
                checkBox = view.findViewById(R.id.model_checked);
                image = view.findViewById(R.id.model_image);
            }
        }

        PhotoAdapter(List<Photo> list) {
            photoList = list;
        }

        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item, parent, false);
            return new PhotoHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final PhotoHolder holder, int position) {
            final Photo model = photoList.get(position);
            Picasso.with(mContext).load(model.url).placeholder(R.drawable.placeholder).into(holder.image);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Store.getInstance().photo = model;
                    redirectTo(PhotoActivity.class);
                }
            });

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.checkBox.isChecked()) {
                        photoListToStore.add(model);
                    } else {
                        photoListToStore.remove(model);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return photoList.size();
        }
    }

    private void getPhotos() {
        Bundle parameters = new Bundle();
        parameters.putString("fields", "images");
        progressBar.setVisibility(View.VISIBLE);
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + Store.getInstance().album.id + "/photos",
                parameters,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        Log.v("TAG", "Facebook Photos response: " + response);
                        progressBar.setVisibility(View.GONE);
                        try {
                            if (response.getError() == null) {
                                JSONObject joMain = response.getJSONObject();
                                if (joMain.has("data")) {
                                    JSONArray jaData = joMain.optJSONArray("data");
                                    for (int i = 0; i < jaData.length(); i++) {
                                        JSONObject joAlbum = jaData.getJSONObject(i);
                                        JSONArray jaImages = joAlbum.getJSONArray("images");
                                        if (jaImages.length() > 0) {
                                            Photo photo = new Photo();
                                            photo.url = jaImages.getJSONObject(0).getString("source");
                                            photoList.add(photo);
                                        }
                                    }
                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                Log.v("TAG", response.getError().toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
        ).executeAsync();
    }
}
