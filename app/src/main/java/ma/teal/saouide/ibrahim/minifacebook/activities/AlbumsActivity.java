package ma.teal.saouide.ibrahim.minifacebook.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ma.teal.saouide.ibrahim.minifacebook.R;
import ma.teal.saouide.ibrahim.minifacebook.activities.Base.BaseActivity;
import ma.teal.saouide.ibrahim.minifacebook.models.Album;
import ma.teal.saouide.ibrahim.minifacebook.tools.ClickListener;
import ma.teal.saouide.ibrahim.minifacebook.tools.Store;

/**
 * Created by Ibrahim on 7/24/18.
 */

public class AlbumsActivity extends BaseActivity {


    Context mContext;
    RecyclerView listView;
    AlbumsActivity.AlbumAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    List<Album> albums = new ArrayList<>();
    RelativeLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        if (AccessToken.getCurrentAccessToken() == null) {
            redirectTo(LoginActivity.class, true);
        } else {
            getAlbums(AccessToken.getCurrentAccessToken());
        }
    }

    private void initView() {
        mContext = this.getBaseContext();
        setContentView(R.layout.activity_albums);
        setupToolbar();
        setTitle("albums");
        progressBar = findViewById(R.id.progressBar);
        listView = findViewById(R.id.list_item);
        layoutManager = new GridLayoutManager(mContext, 2);
        listView.setLayoutManager(layoutManager);
        listView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        listView.setHasFixedSize(true);
        adapter = new AlbumsActivity.AlbumAdapter(albums);
        listView.setAdapter(adapter);
        listView.addOnItemTouchListener(new ClickListener.RecyclerTouchListener(this, listView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Store.getInstance().album = albums.get(position);
                redirectTo(PhotosActivity.class);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    public class AlbumAdapter extends RecyclerView.Adapter<AlbumsActivity.AlbumAdapter.AlbumHolder> {
        List<Album> AlbumList;

        class AlbumHolder extends RecyclerView.ViewHolder {
            TextView label;
            ImageView image;

            AlbumHolder(View view) {
                super(view);
                label = view.findViewById(R.id.model_label);
                image = view.findViewById(R.id.model_image);
            }
        }

        AlbumAdapter(List<Album> list) {
            AlbumList = list;
        }

        @Override
        public AlbumsActivity.AlbumAdapter.AlbumHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item, parent, false);
            return new AlbumsActivity.AlbumAdapter.AlbumHolder(itemView);
        }

        @Override
        public void onBindViewHolder(AlbumsActivity.AlbumAdapter.AlbumHolder holder, int position) {
            Album model = AlbumList.get(position);
            holder.label.setText(model.name);
        }

        @Override
        public int getItemCount() {
            return AlbumList.size();
        }
    }


    private void getAlbums(AccessToken currentAccessToken) {
        progressBar.setVisibility(View.VISIBLE);
        new GraphRequest(
                currentAccessToken,  //your fb AccessToken
                "/" + currentAccessToken.getUserId() + "/albums",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        Log.d("TAG", "Facebook albums: " + response.toString());
                        progressBar.setVisibility(View.GONE);
                        try {
                            if (response.getError() == null) {
                                JSONObject joMain = response.getJSONObject();
                                if (joMain.has("data")) {
                                    albums.clear();
                                    JSONArray jaData = joMain.optJSONArray("data");
                                    for (int i = 0; i < jaData.length(); i++) {
                                        JSONObject joAlbum = jaData.getJSONObject(i);
                                        Album album = new Album();
                                        album.name = joAlbum.optString("name");
                                        album.id = joAlbum.optString("id");
                                        albums.add(album);
                                    }
                                    orderAlbums();
                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                Log.d("Test", response.getError().toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    private void orderAlbums(){
        Collections.sort(albums, new Comparator<Album>(){
            public int compare(Album s1, Album s2) {
                return s1.name.compareToIgnoreCase(s2.name);
            }
        });
    }

}
