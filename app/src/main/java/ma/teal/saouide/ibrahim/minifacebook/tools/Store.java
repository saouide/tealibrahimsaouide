package ma.teal.saouide.ibrahim.minifacebook.tools;


import ma.teal.saouide.ibrahim.minifacebook.models.Album;
import ma.teal.saouide.ibrahim.minifacebook.models.Photo;

/**
 * Created by s2m on 7/24/18.
 */


public class Store {

    private static Store instance = null;

    public Album album;
    public Photo photo;

    public static Store getInstance(){
        if(instance == null) {
            instance = new Store();
        }
        return instance;
    }

    public static void destroy() {
        instance = new Store();
    }
}

